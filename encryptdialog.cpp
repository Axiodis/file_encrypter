#include "encryptdialog.h"
#include "ui_encryptdialog.h"
#include <QFile>
#include <QFileInfo>
#include <QTextStream>
#include <qmath.h>
#include<QMessageBox>
#include <QCloseEvent>

EncryptDialog::EncryptDialog(QString filename, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EncryptDialog)
{
    _filename = filename;
    encrThread = new EncryptionThread(this);

    ui->setupUi(this);

    ui->customValue->setValidator( new QIntValidator(0, 1024, this) );

    ui->customValue->setVisible(false);
    ui->label_3->setVisible(false);
    ui->comboBox->addItem("No");
    for(int i =0; i <= 6; i++){
        ui->comboBox->addItem(QString::number(qPow(2,i))+" MB");
    }
    ui->comboBox->addItem("Custom");

    connect(encrThread,SIGNAL(ProgressPercent(int)),this, SLOT(EncryptionProgress(int)));
    connect(encrThread,SIGNAL(Finished()),this, SLOT(EncryptionSucces()));
    connect(ui->comboBox,SIGNAL(activated(int)),this,SLOT(customOptionSelected(int)));
}

EncryptDialog::~EncryptDialog()
{
    delete ui;
}

void EncryptDialog::closeEvent(QCloseEvent *event)
{
    if(encrThread->isRunning()){
        QMessageBox::StandardButton resBtn =QMessageBox::question( this, "Encryption in progress",
                              tr("The encryption will stop and all the files will be deleted.\nAre you sure?\n"),
                              QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                              QMessageBox::Yes);
        if (resBtn != QMessageBox::Yes) {
                event->ignore();
            } else {
                event->accept();
            }
    }
}

void EncryptDialog::customOptionSelected(int index){
    if(index == (ui->comboBox->count()-1)){
        ui->customValue->setVisible(true);
        ui->label_3->setVisible(true);
    } else {
        ui->customValue->setVisible(false);
        ui->label_3->setVisible(false);
    }
}

void EncryptDialog::EncryptionProgress(int number){
    ui->progressBar->setValue(number);
}

void EncryptDialog::EncryptionSucces(){
    QMessageBox::information(this,"Encryption Finished!","Encryption finished succesfuly!");
    this->close();
}

void EncryptDialog::on_CancelButton_clicked()
{
    this->close();
}

void EncryptDialog::on_OkButton_clicked()
{
    _pass = ui->pass->text();

    if(_pass.isEmpty()){
        QMessageBox::critical(this,"No Password","You have to introduce a password first!");
        return;
    }

    switch (ui->comboBox->currentIndex()) {
    case 0:
            _mega = 0;
        break;
    case 8:
            _mega = ui->customValue->text().toInt();
        break;
    default:
        _mega = qPow(2,ui->comboBox->currentIndex()-1);
        break;
    }

    encrThread->setFile(_filename);
    encrThread->setMega(_mega);
    encrThread->setPass(_pass);
    encrThread->start();
}
