#ifndef ENCRYPTDIALOG_H
#define ENCRYPTDIALOG_H

#include <QDialog>
#include <mainwindow.h>
#include "encryptionthread.h"

namespace Ui {
class EncryptDialog;
}

class EncryptDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EncryptDialog(QString filename,QWidget *parent = 0);
    ~EncryptDialog();

    EncryptionThread *encrThread;

    void closeEvent(QCloseEvent *event);

private slots:
    void on_CancelButton_clicked();

    void on_OkButton_clicked();

    void customOptionSelected(int index);
private:
    QString _filename;
    QString _pass;
    int _mega;
    int _count;
    Ui::EncryptDialog *ui;

public slots:
    void EncryptionProgress(int i);
    void EncryptionSucces();
};


#endif // ENCRYPTDIALOG_H
