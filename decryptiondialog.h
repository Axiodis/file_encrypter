#ifndef DECRYPTIONDIALOG_H
#define DECRYPTIONDIALOG_H

#include <QDialog>
#include <decryptionthread.h>

namespace Ui {
class DecryptionDialog;
}

class DecryptionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DecryptionDialog(QString filename,QWidget *parent = 0);
    ~DecryptionDialog();

private slots:
    void on_cancelButton_clicked();

    void on_okButton_clicked();

public slots:
    void EncryptionProgress(int i);
    void EncryptionSucces();

private:
    Ui::DecryptionDialog *ui;

    QString _filename;
    QString _pass;
    DecryptionThread *decrThread;
};

#endif // DECRYPTIONDIALOG_H
