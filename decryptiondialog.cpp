#include "decryptiondialog.h"
#include "ui_decryptiondialog.h"

DecryptionDialog::DecryptionDialog(QString filename, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DecryptionDialog)
{
    _filename = filename;
    decrThread = new DecryptionThread(this);

    ui->setupUi(this);

    connect(decrThread,SIGNAL(ProgressPercent(int)),this, SLOT(EncryptionProgress(int)));
    connect(decrThread,SIGNAL(Finished()),this, SLOT(EncryptionSucces()));
}

DecryptionDialog::~DecryptionDialog()
{
    delete ui;
}

void DecryptionDialog::EncryptionProgress(int number){
    ui->progress->setValue(number);
}

void DecryptionDialog::EncryptionSucces(){
    QMessageBox::information(this,"Decryption Finished!","Decryption finished succesfuly!");
    this->close();
}

void DecryptionDialog::on_cancelButton_clicked()
{
    this->close();
}

void DecryptionDialog::on_okButton_clicked()
{
    _pass = ui->pass->text();

    if(_pass.isEmpty()){
        QMessageBox::critical(this,"No Password","You have to introduce a password first!");
        return;
    }

    decrThread->setFile(_filename);
    decrThread->setPass(_pass);
    decrThread->start();
}
