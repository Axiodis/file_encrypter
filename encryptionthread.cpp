#include "encryptionthread.h"
#include<QMessageBox>
#include<QException>

EncryptionThread::EncryptionThread(QObject *parent):QThread(parent)
{
    _stop = false;
}

void EncryptionThread::run(){
    while(!_stop){

        QFile file(_filename);
        if (!file.open(QIODevice::ReadOnly)){
            throw "The file couldn't be open!";
        }

        int count;
        float progressPerMbyte;
        if(file.size()<=MBYTE){
            progressPerMbyte = 100;
        } else {
            progressPerMbyte = MBYTE/(float)file.size()*100;
        }
        float progress = 0;

        QDataStream stream(&file);
        QByteArray buffer;
        QString cryptFile = _filename + ".crypt";

        if(_mega == 0){

            QFile fileToWrite(cryptFile);
            fileToWrite.remove();
            fileToWrite.open(QIODevice::Append);

            while(file.bytesAvailable()){

                if(file.bytesAvailable() >= MBYTE){
                    count = MBYTE;
                } else {
                    count = file.bytesAvailable();
                }

                buffer.resize(count);
                stream.readRawData(buffer.data(), count);
                for(int i = 0;i < buffer.size();i++){
                    int poz = i%_pass.size();
                    char x = buffer[i];
                    char toAdd = _pass[poz].unicode();
                    x = x + toAdd;
                    buffer[i] = x;
                }
                fileToWrite.write(buffer);

                progress += progressPerMbyte;
                emit ProgressPercent((int)progress);
            }
            fileToWrite.close();
            _stop = true;
        } else {
            int files;
            QFile fileToWriteIndex(cryptFile+'_'+QString::number(0));
            fileToWriteIndex.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream outFilesIndex(&fileToWriteIndex);

            if(file.size()%(_mega*MBYTE)){
                files = file.size()/(_mega*MBYTE) + 1;
            } else {
                files = file.size()/(_mega*MBYTE);
            }

            outFilesIndex << QString::number(progressPerMbyte) << "\n";

            for(int i=0;i < files; i++){

                QFile fileToWrite(cryptFile+'_'+QString::number(i+1));
                fileToWrite.remove();
                fileToWrite.open(QIODevice::Append);

                QFileInfo infoFileToWrite(fileToWrite);

                outFilesIndex << infoFileToWrite.fileName() << "\n";

                for(int j = 0;j<_mega*MBYTE;j+=MBYTE){

                    if(file.bytesAvailable() >= MBYTE){
                        count = MBYTE;
                    } else {
                        count = file.bytesAvailable();
                    }

                    buffer.resize(count);
                    stream.readRawData(buffer.data(), count);
                    for(int k = 0;k < buffer.size();k++){
                        int poz = k % _pass.size();
                        char x = buffer[k];
                        char toAdd = _pass[poz].unicode();
                        x = x + toAdd;
                        buffer[k] = x;
                    }
                    fileToWrite.write(buffer);
                    progress += progressPerMbyte;
                    emit ProgressPercent((int)progress);
                }
                fileToWrite.close();
                _stop = true;
            }
        }
    }
    emit ProgressPercent(100);
    emit Finished();
}
