#ifndef ENCRYPTIONTHREAD_H
#define ENCRYPTIONTHREAD_H

#include<QThread>
#include<QtCore>
#include<QMessageBox>
#include<QWindow>


class EncryptionThread : public QThread
{
    Q_OBJECT

private:
    const int MBYTE = 1024*1024;
    bool _stop;
    QString _filename;
    QString _pass;
    int _mega;
    QFile fileToWrite;

public:
    explicit EncryptionThread(QObject *parent = 0);
    void run();

    void setFile(QString file){
        _filename = file;
    }

    void setPass(QString pass){
        _pass = pass;
    }

    void setMega(int mega){
        _mega = mega;
    }

signals:
    void ProgressPercent(int i);
    void Finished();
public slots:

};

#endif // ENCRYPTIONTHREAD_H
