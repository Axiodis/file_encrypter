#include "decryptionthread.h"
#include <QFileInfo>

DecryptionThread::DecryptionThread(QObject *parent):QThread(parent)
{
    _stop = false;
}

void DecryptionThread::run(){
    while(!_stop){

        QFile file(_filename);
        if (!file.open(QIODevice::ReadOnly)){
            throw "The file couldn't be open!";
        }

        int count = MBYTE;
        float progressPerMbyte;
        float progress = 0;
        QString path = QFileInfo(_filename).path()+'/' ;


        if(_filename[_filename.length()-1] == '0'){

            QTextStream indexFile(&file);
            progressPerMbyte = indexFile.readLine().toFloat();

            QByteArray buffer;
            QString decryptFile = _filename.remove(".crypt_0");
            QFile fileToWrite(decryptFile);
            fileToWrite.remove();
            fileToWrite.open(QIODevice::WriteOnly|QIODevice::Append);

            while(!indexFile.atEnd()) {
                QString filePart = indexFile.readLine();

                QFile fileToRead(path + filePart);
                if (!fileToRead.open(QIODevice::ReadOnly)){
                    throw "The file couldn't be open!";
                }

                QDataStream stream(&fileToRead);

                while(fileToRead.bytesAvailable()){

                    if(fileToRead.bytesAvailable() < MBYTE){
                        count = fileToRead.bytesAvailable();
                    }

                    buffer.resize(count);
                    stream.readRawData(buffer.data(), count);

                    for(int i = 0;i < buffer.size();i++){
                        int poz = i%_pass.size();
                        buffer[i] = buffer[i] - _pass[poz].unicode();
                    }

                    fileToWrite.write(buffer);

                    progress += progressPerMbyte;
                    emit ProgressPercent((int)progress);
                }
                fileToRead.close();
            }
            fileToWrite.close();
            file.close();
            _stop = true;

        } else {

            if(file.size()<=MBYTE){
                progressPerMbyte = 100;
            } else {
                progressPerMbyte = MBYTE/(float)file.size()*100;
            }

            QDataStream stream(&file);
            QByteArray buffer;
            QString decryptFile = _filename.remove(".crypt");

            QFile fileToWrite(decryptFile);
            fileToWrite.remove();
            fileToWrite.open(QIODevice::WriteOnly|QIODevice::Append);


            while(file.bytesAvailable()){

                if(file.bytesAvailable() < MBYTE){
                    count = file.bytesAvailable();
                }

                buffer.resize(count);
                stream.readRawData(buffer.data(), count);

                for(int i = 0;i < buffer.size();i++){
                    int poz = i%_pass.size();
                    buffer[i] = buffer[i] - _pass[poz].unicode();
                }

                fileToWrite.write(buffer);

                progress += progressPerMbyte;
                emit ProgressPercent((int)progress);
            }
            fileToWrite.close();
            file.close();
            _stop = true;
        }
    }
    emit ProgressPercent(100);
    emit Finished();
}
