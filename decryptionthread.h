#ifndef DECRYPTIONTHREAD_H
#define DECRYPTIONTHREAD_H

#include<QThread>
#include<QtCore>
#include<QMessageBox>
#include<QWindow>

class DecryptionThread: public QThread
{
    Q_OBJECT

public:
    explicit DecryptionThread(QObject *parent = 0);
    void run();

    void setFile(QString file){
        _filename = file;
    }

    void setPass(QString pass){
        _pass = pass;
    }

private:
    QString _pass;
    QString _filename;
    bool _stop;
    const int MBYTE = 1024 * 1024;

signals:
    void ProgressPercent(int i);
    void Finished();
};

#endif // DECRYPTIONTHREAD_H
