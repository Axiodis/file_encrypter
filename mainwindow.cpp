#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>
#include <QTextStream>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QFile f(QDir::currentPath()+"/qdarkstyle/style.qss");
    if (!f.exists())
    {
        QMessageBox::critical(this,"Missing File",QDir::currentPath()+"/qdarkstyle/style.qss");
    }
    else
    {
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
    }

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_open_clicked()
{
     _filename=QFileDialog::getOpenFileName(
                 this,
                 tr("Open File"),
                 QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),
                 "All Files (*.*);;Text Files (*.txt)");
    ui->path->setText(_filename);
}

void MainWindow::on_EncryptButton_clicked()
{
    if(!_filename.isEmpty()){
        EncryptDialog encryptDialog(_filename);
        encryptDialog.setModal(true);
        encryptDialog.exec();
    } else {
        QMessageBox::critical(this,"Missing File","You have to chose a file first!");
    }
}

void MainWindow::on_DecryptButton_clicked()
{
    if(!_filename.isEmpty()){
        DecryptionDialog decryptDialog(_filename);
        decryptDialog.setModal(true);
        decryptDialog.exec();
    } else {
        QMessageBox::critical(this,"Missing File","You have to chose a file first!");
    }
}
